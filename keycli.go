package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"github.com/joho/godotenv"
	"log"
	"os"
)

func main() {
	err := godotenv.Load("input.env")
	if err != nil {
		log.Fatal("Error loading the input.env file")
	}

	privkey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		fmt.Println("Cannot generate RSA key")
	}
	pubkey := &privkey.PublicKey
	// dump private key to file
	var privateKeyBytes []byte = x509.MarshalPKCS1PrivateKey(privkey)
	privateKeyBlock := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: privateKeyBytes,
	}
	output_file := fmt.Sprintf("%s.key", os.Getenv("CommonName"))
	privatePem, err := os.Create(output_file)
	if err != nil {
		fmt.Printf("error when create private.pem: %s \n", err)
		os.Exit(1)
	}
	err = pem.Encode(privatePem, privateKeyBlock)
	if err != nil {
		fmt.Printf("error when encode private pem: %s \n", err)
		os.Exit(1)
	}

	// dump public key to file
	publicKeyBytes, err := x509.MarshalPKIXPublicKey(pubkey)
	if err != nil {
		fmt.Printf("error when dumping publickey: %s \n", err)
		os.Exit(1)
	}
	publicKeyBlock := &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: publicKeyBytes,
	}
	output_file = fmt.Sprintf("%s.pub", os.Getenv("CommonName"))
	publicPem, err := os.Create(output_file)
	if err != nil {
		fmt.Printf("error when creating public.pem: %s \n", err)
		os.Exit(1)
	}
	err = pem.Encode(publicPem, publicKeyBlock)
	if err != nil {
		fmt.Printf("error when encoding public pem: %s \n", err)
		os.Exit(1)
	}

	csrTemplate := x509.CertificateRequest{
		SignatureAlgorithm: x509.SHA256WithRSA,
		Subject: pkix.Name{
			CommonName:         os.Getenv("CommonName"),
			Country:            []string{os.Getenv("Country")},
			StreetAddress:      []string{os.Getenv("StreetAddress")},
			Locality:           []string{os.Getenv("Locality")},
			Organization:       []string{os.Getenv("Organization")},
			OrganizationalUnit: []string{os.Getenv("OrganizationalUnit")},
		},
	}

	csr, err := x509.CreateCertificateRequest(rand.Reader, &csrTemplate, privkey)
	if err != nil {
		fmt.Println("Cannot generate CSR")
	}

	output_file = fmt.Sprintf("%s.csr", os.Getenv("CommonName"))
	csrPemFile, err := os.Create(output_file)
	if err != nil {
		fmt.Printf("Error when creating the CSR file: %s \n", err)
		os.Exit(1)
	}
	err = pem.Encode(csrPemFile, &pem.Block{
		Type: "CERTIFICATE REQUEST", Bytes: csr,
	})
	if err != nil {
		fmt.Println("Could ")
	}
}
